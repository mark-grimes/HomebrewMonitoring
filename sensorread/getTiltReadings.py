#! /usr/bin/env python3
#
# Note that this file requires root privileges or setcap 'cap_net_raw,cap_net_admin+eip' to run
#

from beacontools import BeaconScanner, IBeaconFilter

# # You can scan for all Tilts and get the UUID with:
# from beacontools import IBeaconAdvertisement
# scanner = BeaconScanner(callback, packet_filter=IBeaconAdvertisement)

# I got a list of these UUIDs from https://kvurd.com/blog/tilt-hydrometer-ibeacon-data-format/
tiltUUID = {
    "red"    : "a495bb10-c5b1-4b44-b512-1370f02d74de",
    "green"  : "a495bb20-c5b1-4b44-b512-1370f02d74de",
    "black"  : "a495bb30-c5b1-4b44-b512-1370f02d74de",
    "orange" : "a495bb50-c5b1-4b44-b512-1370f02d74de",
    "purple" : "a495bb40-c5b1-4b44-b512-1370f02d74de",
    "blue"   : "a495bb60-c5b1-4b44-b512-1370f02d74de",
    "yellow" : "a495bb70-c5b1-4b44-b512-1370f02d74de",
    "pink"   : "a495bb80-c5b1-4b44-b512-1370f02d74de"
}

colourLookup = {} # create a reverse lookup dict
for key, value in tiltUUID.items() :
    colourLookup[ value ] = key

uuid = tiltUUID["red"]

# global_stuff = {}

def callback(bt_addr, rssi, packet, additional_info):
    # For a Tilt packet.major is the temperature in Fahrenheit and packet.minor is the
    # specific gravity times a thousand.
    # print("<%s, %d> %s %s" % (bt_addr, rssi, packet, additional_info))
    print( "Tilt:", colourLookup[ packet.uuid ], "Temperature:", (packet.major - 32) * 5 / 9, "SG:", packet.minor / 1000 )
    #global_stuff['bt_addr'] = bt_addr
    #global_stuff['rssi'] = rssi
    #global_stuff['packet'] = packet
    #global_stuff['additional_info'] = additional_info


scanner = BeaconScanner(callback, device_filter=IBeaconFilter(uuid=uuid))

scanner.start()


import time
time.sleep(5)
scanner.stop()
