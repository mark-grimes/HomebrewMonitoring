#! /usr/bin/env python3
#
# Script to write sensor values to the database
#
import MySQLdb
import time

class BrewPiDB(object) :

    ConnectionException = MySQLdb._exceptions.OperationalError

    @staticmethod
    def createOrRetry( host = "localhost", retryDelays = None ) :
        """ Try to create a database connection. If it fails, wait a set amount of time and try again. """
        if retryDelays == None : retryDelays = [ 32, 16, 8, 4, 2 ] # Seconds to wait before next retry
        while True:
            try:
                database = BrewPiDB( host = host )
                return database
            except BrewPiDB.ConnectionException as error:
                if len(retryDelays) == 0 :
                    # We've retried connecting as many times as we can. Pass the exception on.
                    raise error
                else :
                    delay = retryDelays.pop()
                    print("MySQLdb connection to", host, "failed. Waiting for", delay, "seconds before retrying.")
                    time.sleep(delay)

    def __init__( self, host = "localhost" ) :
        retryDelays = [ 32, 16, 8, 4, 2 ] # Seconds to wait before next retry
        self._db = MySQLdb.connect( host=host, user="brewpi", password="brewpi", database="brewpi" )
        self._cursor = self._db.cursor()

    def write( self, tilt_fahrenheit, tilt_sg_E3, ambient_celsius = None, ambient_humidity = None, reservoir_celsius = None ) :
        sqlStatement = "INSERT INTO fermentor_conditions ("
        sqlStatementTail = " VALUES ("
        values = []
        needComma = False
        for value, name in [ (tilt_fahrenheit,"tilt_fahrenheit"), (tilt_sg_E3,"tilt_sg_E3"), (ambient_celsius,"ambient_celsius"), (ambient_humidity,"ambient_humidity"), (reservoir_celsius,"reservoir_celsius") ]:
            if value != None :
                if needComma :
                    sqlStatement += ","
                    sqlStatementTail += ","
                sqlStatement += name
                sqlStatementTail += "%s"
                values.append( value )
                needComma = True
        sqlStatement = sqlStatement + ")" + sqlStatementTail + ")"
        self._cursor.execute( sqlStatement, values )
        self._db.commit()

if __name__ == "__main__" :
    from getAmbientTemperature import BufferedHIH6130
    from beacontools import BeaconScanner, IBeaconFilter
    import datetime

    uuid = "a495bb10-c5b1-4b44-b512-1370f02d74de"
    sensor = BufferedHIH6130()
    # In case the database hasn't come up yet, we run a sleep-retry loop a few times.
    database = BrewPiDB.createOrRetry()

    maxCadence = datetime.timedelta( seconds = 30 )
    lastRecording = datetime.datetime.now()

    def iBeaconCallback(bt_addr, rssi, packet, additional_info):
        global lastRecording
        thisRecording = datetime.datetime.now()
        if (thisRecording - lastRecording) < maxCadence :
            return # Don't record more frequently than maxCadence
        tilt_fahrenheit = packet.major
        tilt_sg_E3 = packet.minor
        ambient_celsius = sensor.temperature()
        ambient_humidity = sensor.humidity()
        database.write( tilt_fahrenheit, tilt_sg_E3, ambient_celsius, ambient_humidity )
        lastRecording = thisRecording

    scanner = BeaconScanner( iBeaconCallback, device_filter = IBeaconFilter( uuid = uuid ) )
    scanner.start()
