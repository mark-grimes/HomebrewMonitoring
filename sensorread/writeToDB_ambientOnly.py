#! /usr/bin/env python3
#
# Script to write sensor values to the database
#
import MySQLdb
import time
from writeToDB import BrewPiDB

if __name__ == "__main__" :
    from getAmbientTemperature import BufferedHIH6130
    import datetime, time

    sensor = BufferedHIH6130()
    # In case the database hasn't come up yet, we run a sleep-retry loop a few times.
    database = BrewPiDB.createOrRetry()

    maxCadence = datetime.timedelta( seconds = 30 )

    print( datetime.datetime.now(), "Started logging ambient temperature and humidity ONLY" )

    while True:
        tilt_fahrenheit = 0
        tilt_sg_E3 = 0
        ambient_celsius = sensor.temperature()
        ambient_humidity = sensor.humidity()

        database.write( tilt_fahrenheit, tilt_sg_E3, ambient_celsius, ambient_humidity )

        time.sleep( maxCadence.total_seconds() )
