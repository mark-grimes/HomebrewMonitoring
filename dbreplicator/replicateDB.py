#! /usr/bin/env python3
#
# Script to copy rows from one database to another, where the primary key is a timestamp.
# The scripts sleeps for a set amount of time, then only insert rows with a larger timestamp
# than previously existed in the target DB.
#

import MySQLdb

def update( sourceDB, destinationDB ) :
    batchSize = 1000

    destCursor = destinationDB.cursor()
    destCursor.execute( "SELECT max(timestamp) FROM fermentor_conditions" )
    lastTimestamp = destCursor.fetchone()[ 0 ]

    srcCursor = sourceDB.cursor()
    if lastTimestamp == None :
        numberOfRows = srcCursor.execute( "SELECT * FROM fermentor_conditions" )
    else :
        numberOfRows = srcCursor.execute( "SELECT * FROM fermentor_conditions WHERE timestamp > %s", (lastTimestamp,) )
    print( "Replicating", numberOfRows, "rows to the destination DB in batches of", batchSize )

    # if numberOfRows == 0 : return

    while True :
        rows = srcCursor.fetchmany( batchSize )
        if len(rows) == 0 :
            break # Must have finished all the batches

        # TODO: Find a way to query the DBs for column names, so that I don't have to hard code them
        destCursor.executemany( """INSERT INTO fermentor_conditions
                                    (timestamp , tilt_fahrenheit, tilt_sg_E3, ambient_celsius, ambient_humidity, reservoir_celsius)
                                    VALUES (%s,%s,%s,%s,%s,%s)""", rows )

    destinationDB.commit()
    sourceDB.commit()
    destCursor.close()
    srcCursor.close()

if __name__ == "__main__" :
    import time, sys

    sleepTime = 180 # Wait this many seconds between trying to update

    if len(sys.argv) != 7 :
        print( "Usage: <source host> <source user> <source password> <dest host> <dest user> <dest password>" )
        raise Exception("Incorrect number of command line arguments")

    local = MySQLdb.connect( host = sys.argv[1], user = sys.argv[2], password = sys.argv[3], database="brewpi" )
    remote = MySQLdb.connect( host = sys.argv[4], user = sys.argv[5], password = sys.argv[6], database="brewpi" )

    # Run continually, updating the database everytime we wake up
    while True :
        update( local, remote )
        time.sleep( sleepTime )
