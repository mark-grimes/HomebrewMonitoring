-- This configuration should be applied to the `mysql` database to set user permissions
CREATE DATABASE brewpi;
GRANT SELECT, INSERT ON brewpi.* TO 'brewpi'@'%' IDENTIFIED BY 'brewpi'; -- CHANGE THE PASSWORD!!!
GRANT SELECT ON brewpi.* TO 'grafana'@'%' IDENTIFIED BY 'grafana';       -- CHANGE THE PASSWORD!!!
USE brewpi;
CREATE TABLE fermentor_conditions(
    timestamp TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    tilt_fahrenheit INTEGER,
    tilt_sg_E3 INTEGER,
    ambient_celsius FLOAT,
    ambient_humidity FLOAT,
    reservoir_celsius FLOAT,
    PRIMARY KEY (timestamp)
);
CREATE TABLE brews(
    id INT NOT NULL AUTO_INCREMENT,
    start_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    finish_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    name VARCHAR(255),
    description VARCHAR(255),
    start_sg_E3 INTEGER,
    target_sg_E3 INTEGER,
    PRIMARY KEY (id)
);
