# HomebrewMonitoring

Project running on a Raspberry Pi Zero W to record sensor readings during fermentation and display in a dashboard. Collects data from a [Tilt hydrometer](https://tilthydrometer.com) and a temperature/humidity sensor connected to the R-Pi over I2C.
